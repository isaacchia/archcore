﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Model
{
    class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
    }
}
