﻿using ArchData.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ArchData.Service
{
    public class AuthService: IAuthService
    {
       public Token ParseToken(string code)
        {
            Token token = new Token();
            var jwtHandler = new JwtSecurityTokenHandler();
            var readableToken = jwtHandler.CanReadToken(code);
            if (readableToken == true)
            {
                var jwtToken = jwtHandler.ReadJwtToken(code);
                var claims = jwtToken.Claims;

                foreach (Claim c in claims)
                {
                    if (c.Type == "userid")
                    {
                        token.userid = Convert.ToInt32(c.Value);
                    }
                    if (c.Type == "emailaddress")
                    {
                        token.emailaddress = c.Value;
                    }
                    if (c.Type == "webbrandcode")
                    {
                        token.webbrandcode = c.Value;
                    }
                    if (c.Type == "groupcode")
                    {
                        token.groupcode = c.Value;
                    }
                    if (c.Type == "exp")
                    {
                        token.exp = Convert.ToInt64(c.Value);
                    }
                }
            }
            return token;
        }
    }
}
