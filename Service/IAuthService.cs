﻿using ArchData.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Service
{
    public interface IAuthService
    {
        public Token ParseToken(string code);
    }
}
