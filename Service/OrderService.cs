﻿using ArchData.Model;
using ArchData.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArchData.Service
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<IEnumerable<OrderHeader>> getOrders()
        {
            return await _orderRepository.GetAll();
        }


        public async Task<OrderHeader> getOrderById(int orderId)
        {

            SqlParam param1 = new SqlParam("id", orderId);
            LogicalSqlParam masterLogicalParam = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { param1 });

            //await _orderRepository.UpdateOrderHeader(new Dictionary<String, dynamic> { { "amount", 780 } }, masterLogicalParam);
            return await _orderRepository.GetById(orderId);
        }


        public async Task<dynamic> getOrderByIdWithCustomField(int orderId)
        {
            SqlParam param1 = new SqlParam("id", orderId);
            LogicalSqlParam masterLogicalParam = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { param1 });

            return await _orderRepository.Select(new Dictionary<String, String> { { "id", "idField" }, { "userid", "user" } }, masterLogicalParam, new List<string> { "id" });
        }
    }

}
