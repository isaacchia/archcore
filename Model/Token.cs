﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Model
{
    public class Token
    {
        public int userid { get; set; }
        public string emailaddress { get; set; }
        public string webbrandcode { get; set; }
        public string groupcode { get; set; }
        public long exp { get; set; }
    }
}
