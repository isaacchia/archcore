﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

public abstract class BaseRepository
{
    private readonly IConfiguration _configuration;
    private readonly string _ConnectionString;
    protected BaseRepository(IConfiguration configuration)
    {
        _configuration = configuration;
        _ConnectionString = _configuration.GetConnectionString("MySQLConnection");
    }


    // use for buffered queries that return a type
    protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
    {
        try
        {
            await using var connection = new MySqlConnection(_ConnectionString);
            await connection.OpenAsync();
            return await getData(connection);
        }
        catch (TimeoutException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
        }
        catch (MySqlException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
        }
    }

    // use for buffered queries that do not return a type
    protected async Task WithConnection(Func<IDbConnection, Task> getData)
    {
        try
        {
            await using var connection = new MySqlConnection(_ConnectionString);
            await connection.OpenAsync();
            await getData(connection);
        }
        catch (TimeoutException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
        }
        catch (MySqlException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
        }
    }

    // use for non-buffered queries that return a type
    protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
    {
        try
        {
            await using var connection = new MySqlConnection(_ConnectionString);
            await connection.OpenAsync();
            var data = await getData(connection);
            return await process(data);
        }
        catch (TimeoutException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL timeout", GetType().FullName), ex);
        }
        catch (MySqlException ex)
        {
            throw new Exception(String.Format("{0}.WithConnection() experienced a SQL exception (not a timeout)", GetType().FullName), ex);
        }
    }


    public static string ConstructGenericFields(IDictionary<string, string> selectFields)
    {
        string result = string.Join(" , ", selectFields.Select(x => $"{x.Key} AS {x.Value}"));
        return result;
    }

    public static string ConstructGenericUpdateFields(IDictionary<string, dynamic> updateFields)
    {
      

        if (updateFields == null)
        {
            return String.Empty;
        }
        else
        {
            return string.Join(" , ", updateFields.Select(x =>
            {
                if (x.Value is int)
                {
                    return $"{x.Key} = {x.Value}";
                }
                else if (x.Value is bool)
                {
                    int value = x.Value == true ? 1 : 0;
                    return $"{x.Key} = {value}";
                }
                else
                {
                    return $"{x.Key} = '{x.Value}'";
                }

            }));
        }

    }
    public static string ConstructGenericWhereFields(LogicalSqlParam logicalSqlParam)
    {
        string result = "";
        List<string> tmp = new List<string>();

        if (logicalSqlParam == null)
        {
            return String.Empty;
        }
        else
        {
            logicalSqlParam.ParamList.ForEach(x =>
            {
                if (x.GetType() == typeof(LogicalSqlParam))
                {
                    tmp.Add(ConstructGenericWhereFields(x));
                }
                else
                {
                    if (x.Value is int)
                    {
                        tmp.Add($"{x.Field} {x.ArithmeticOperator} {x.Value}");
                    }
                    else if (x.Value is bool)
                    {
                        int value = x.Value == true ? 1 : 0;
                        tmp.Add($"{x.Field} {x.ArithmeticOperator} {value}");
                    }
                    else
                    {
                        tmp.Add($"{x.Field} {x.ArithmeticOperator} '{x.Value}'");
                    }

                }

            });

            result = string.Join($" {logicalSqlParam.LogicalOperator} ", tmp);
            return $" ({result}) ";
        }


    }

    public static string ConstructSQLStatement(string statement, string variable)
    {
        return variable != "" ? statement : "";
    }

}

public class SqlParam
{

    public string Field { get; set; }
    public dynamic Value { get; set; }
    public string ArithmeticOperator { get; set; }

    public SqlParam(string field, dynamic value, string arithmeticOperator = "=")
    {
        Field = field;
        Value = value;
        ArithmeticOperator = arithmeticOperator;
    }
}

public class LogicalSqlParam
{
    public LOGICAL_OPERATOR LogicalOperator { get; set; }
    public List<dynamic> ParamList { get; set; }

    public LogicalSqlParam(LOGICAL_OPERATOR logicalOperator, List<dynamic> paramList)
    {
        ParamList = paramList; 
        LogicalOperator = logicalOperator;
    }
}

public enum LOGICAL_OPERATOR
{
    AND,
    OR
}
