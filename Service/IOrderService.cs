﻿using ArchData.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArchData.Service
{
    public interface IOrderService
    {
        Task<IEnumerable<OrderHeader>> getOrders();
        Task<OrderHeader> getOrderById(int orderId);
        Task<dynamic> getOrderByIdWithCustomField(int orderId);
    }
}
