﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Model
{
    [Table("OrderHeader")]
    public class OrderHeader
    {
        [Key]
        public int id { get; set; }
        public int userid { get; set; }
        public double amount { get; set; }
        public string status { get; set; }
        public DateTime created_date { get; set; }

        [Write(false)]
        public List<OrderItem> orderitems { get; set; }

    }
}
