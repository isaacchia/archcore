﻿using ArchData.Model;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchData.Repository
{
    public class OrderRepository : BaseRepository, IOrderRepository
    {


        public OrderRepository(IConfiguration configuration) : base(configuration)
        { 
        
        }

        public async ValueTask<IEnumerable<OrderHeader>> GetAll()
        {


            string test = ConstructGenericWhereFields(testParam());

            return await WithConnection(async conn =>
            {
                var query = await conn.QueryAsync<OrderHeader, OrderItem, OrderHeader>(
                    "Select * from OrderHeader h join OrderItem i on i.orderheaderid = h.id",
                    (header, item) => {
                        if (header.orderitems == null)
                        {
                            header.orderitems = new List<OrderItem>();
                        }
                        header.orderitems.Add(item);
                        return header;
                    });


                return query;
            });

        }

        public async ValueTask<OrderHeader> GetById(int id)
        {

            
           // string test = ConstructGenericWhereFields(testParam());

            return await WithConnection(async conn =>
            {
                var query = await conn.QueryAsync<OrderHeader, OrderItem, OrderHeader>(
                    "Select * from OrderHeader h join OrderItem i on i.orderheaderid = h.id where h.id= @Id", 
                    (header, item) => {       
                        if (header.orderitems == null)
                        {
                            header.orderitems = new List<OrderItem>();
                        }
                        header.orderitems.Add(item);
                        return header; 
                    }, new { Id = id });
              

                    return query.FirstOrDefault();
            });

        }

        private LogicalSqlParam testParam()
        {
            SqlParam param1 = new SqlParam("id", "123");
            SqlParam param2 = new SqlParam("type", "1");

            LogicalSqlParam logicalSqlParam1 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param1, param2 });


            SqlParam param3 = new SqlParam("id", "222");
            SqlParam param4 = new SqlParam("type", "2");

            LogicalSqlParam logicalSqlParam2 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param3, param4 });

            LogicalSqlParam logicalSqlParamAll  = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { logicalSqlParam1, logicalSqlParam2 });
            //SqlParam param2 = new SqlParam("name", "isaac");   

            //SqlParam param11 = new SqlParam("id", "14");
            //SqlParam param22 = new SqlParam("name", "isaac-2");        

            //SqlParam param3 = new SqlParam("id", "5");
            //SqlParam param4 = new SqlParam("name", "goat");      

            //SqlParam param33 = new SqlParam("id", "15");
            //SqlParam param44 = new SqlParam("name", "goat-2");


            //LogicalSqlParam logicalSqlParam1 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param1, param2 });
            //LogicalSqlParam logicalSqlParam2 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param11, param22 });
            //LogicalSqlParam logicalSqlParam3 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param3, param4 });
            //LogicalSqlParam logicalSqlParam4 = new LogicalSqlParam(LOGICAL_OPERATOR.AND, new List<dynamic> { param33, param44 });


            //LogicalSqlParam subLogicalParam1 = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { logicalSqlParam1, logicalSqlParam2 });
            //LogicalSqlParam subLogicalParam2 = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { logicalSqlParam3, logicalSqlParam4 });

            //LogicalSqlParam masterLogicalParam = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { subLogicalParam1, subLogicalParam2 });
            LogicalSqlParam masterLogicalParam = new LogicalSqlParam(LOGICAL_OPERATOR.OR, new List<dynamic> { param1 });

            return masterLogicalParam;
        }

        public async Task<IEnumerable<OrderHeader>> GetByUser(int userId)
        {

            return await WithConnection(async conn =>
            {
                var query = await conn.QueryAsync<OrderHeader, OrderItem, OrderHeader>(
                    "Select * from OrderHeader h join OrderItem i on i.orderheaderid = h.id where h.userid= @userId",
                    (header, item) => {
                        if (header.orderitems == null)
                        {
                            header.orderitems = new List<OrderItem>();
                        }
                        header.orderitems.Add(item);
                        return header;
                    }, new { userId = userId });


                return query;
            });

        }


        public async Task<IEnumerable<dynamic>> Select(
            IDictionary<string, string> selectMapping,
            LogicalSqlParam whereMapping,
            List<string> orderByMapping = null,
            List<string> groupByMapping = null)
        {

            orderByMapping ??= new List<string>();
            groupByMapping ??= new List<string>();

            string selectColumn = ConstructGenericFields(selectMapping);
            string whereResult = ConstructGenericWhereFields(whereMapping);
            string orderByColumn = String.Join(",", orderByMapping);
            string groupByColumn = String.Join(",", groupByMapping);


            string selectStatement = $"SELECT {selectColumn} FROM OrderHeader ";
            string whereStatement = ConstructSQLStatement($"WHERE {whereResult}", whereResult);
            string orderByStatement = ConstructSQLStatement($"ORDER BY {orderByColumn} ", orderByColumn);
            string groupByStatement = ConstructSQLStatement($"ORDER BY {groupByColumn} ", groupByColumn);

            return await WithConnection(async conn =>
            {
                return await conn.QueryAsync<dynamic>($"{selectStatement} {whereStatement} {orderByStatement} {groupByStatement}");
            });

        }


        public async Task<int> AddOrderHeader(OrderHeader entity)
        {
            return await WithConnection(async conn =>
            {
                return await conn.InsertAsync(entity);
            });
        }

        public async Task<int> AddOrderItem(OrderItem entity)
        {
            return await WithConnection(async conn =>
            {
                return await conn.InsertAsync(entity);
            });
        }

        public async Task UpdateOrderHeader(OrderHeader entity)
        {
            await WithConnection(async conn =>
            {
                await conn.UpdateAsync(entity);
            });
        }


        public async Task UpdateOrderHeader(
           IDictionary<string, dynamic> updateMapping,
           LogicalSqlParam whereMapping)
        {

           
            string updateColumn = ConstructGenericUpdateFields(updateMapping);
            string whereResult = ConstructGenericWhereFields(whereMapping);



            string updateStatement = $"UPDATE OrderHeader SET {updateColumn}";
            string whereStatement = $"WHERE {whereResult}";
            await WithConnection(async conn =>
            {
                await conn.ExecuteAsync($"{updateStatement} {whereStatement}");
            });

        }


    }





}
