﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Service
{
   public interface ILogService
    {
        public void LogError(string message, string reference);

        public void Log(string message, string reference);
    }
}
