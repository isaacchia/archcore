﻿using ArchData.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArchData.Repository
{
    public interface IOrderRepository
    {
        ValueTask<IEnumerable<OrderHeader>> GetAll();
        ValueTask<OrderHeader> GetById(int id);
        Task<IEnumerable<OrderHeader>> GetByUser(int userId);
        Task<IEnumerable<dynamic>> Select(
          IDictionary<string, string> selectMapping,
          LogicalSqlParam whereMapping,
          List<string> orderByMapping = null,
            List<string> groupByMapping = null);
        Task<int> AddOrderHeader(OrderHeader entity);
        Task<int> AddOrderItem(OrderItem entity);
        Task UpdateOrderHeader(
           IDictionary<string, dynamic> updateMapping,
           LogicalSqlParam whereMapping);
    }
}
