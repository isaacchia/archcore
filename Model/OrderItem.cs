﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArchData.Model
{
    [Table("OrderItem")]
    public class OrderItem
    {
        [Key]
        public int id { get; set; }
        public int orderheaderid { get; set; }
        public int productid { get; set; }
    }
}
